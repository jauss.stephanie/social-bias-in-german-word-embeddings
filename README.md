# Social Bias in German Word Embeddings

Code accompanying my Bachelor's thesis "Untersuchung von gesellschaftlichem Bias in deutschen Word Embeddings"

## How to Run

Run `jupyter notebook` after having installed conda and notebook.
